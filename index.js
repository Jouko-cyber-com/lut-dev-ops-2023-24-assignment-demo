const app = require("./app");
const bodyParser = require('body-parser');

const port = process.env.PORT || 3000;

app.use(bodyParser.json());

app.post('/webhook', (req, res) => {
    console.log('Received webhook:', req.body);
    res.sendStatus(200);
});

app.listen(port, () => {
  console.log(`Company X app listening at ${port}`);
});
